package edu.msstate.nsparc.batch.processors;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.stereotype.Component;

@Component("batchIncrementer")
public class BatchJobIdIncrementer implements JobParametersIncrementer {

	public JobParameters getNext(JobParameters parameters) {
		System.out.println("got job parameters: " + parameters);
		if (parameters == null || parameters.isEmpty()) {
			return new JobParametersBuilder().addLong("run.id", 1L)
					.toJobParameters();
		}
		long id = parameters.getLong("run.id", 1L) + 1;
		return new JobParametersBuilder().addLong("run.id", id)
				.toJobParameters();
	}

}
