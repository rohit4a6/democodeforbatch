package edu.msstate.nsparc.batch.processors;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import edu.msstate.nsparc.batch.info.JobInfoHolder;

public class JobInfoItemWriter implements ItemWriter<JobInfoHolder> {

	private static final Logger logger = Logger.getLogger(JobInfoItemWriter.class);

	public void write(List<? extends JobInfoHolder> infoHolders) throws Exception {
		for (JobInfoHolder info : infoHolders) {
			logger.info("JobInfoItemWriter : " + info.toString());
		}
	}

}