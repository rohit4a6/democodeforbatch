package edu.msstate.nsparc.batch.processors;

import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import edu.msstate.nsparc.batch.info.BatchInputFields;

@Component("batchInputFieldExtractor")
@Scope("step")
public class BatchInputFieldExtractor implements FieldExtractor<BatchInputFields> {

	public Object[] extract(BatchInputFields inputRecord) {
		return new Object[] { emptyIfNull(inputRecord.getCaseId()), emptyIfNull(inputRecord.getClientId()),
				emptyIfNull(inputRecord.getSsn()) };
	}

	private String emptyIfNull(String s) {
		return s != null ? s : "";
	}

}
