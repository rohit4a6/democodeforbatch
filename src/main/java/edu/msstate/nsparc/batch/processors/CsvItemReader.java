package edu.msstate.nsparc.batch.processors;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;

import edu.msstate.nsparc.batch.info.BatchInputFields;
import edu.msstate.nsparc.batch.info.JobInfoHolder;

public class CsvItemReader implements ItemReader<JobInfoHolder>, ItemStream// ,
																			// StepExecutionListener
{

	// private int count = 0;
	// private StepExecution stepExecution;

	private ItemReader<BatchInputFields> delegate;

	public synchronized JobInfoHolder read() throws Exception {
		BatchInputFields input = delegate.read();
		JobInfoHolder infoHolder = null;
		if (input != null) {
			infoHolder = new JobInfoHolder();
			infoHolder.setJobName(System.getProperties().getProperty("batchJobName"));
			// ThreadUtils.writeThreadExecutionMessage("read", product);
			// return infoHolder;
		}
		return infoHolder;
	}

	// @BeforeStep

	public ItemReader<BatchInputFields> getDelegate() {
		return delegate;
	}

	public void setDelegate(ItemReader<BatchInputFields> delegate) {
		this.delegate = delegate;
	}

	// Stream

	public void close() throws ItemStreamException {
		if (this.delegate instanceof ItemStream) {
			((ItemStream) this.delegate).close();
		}
	}

	public void open(ExecutionContext context) throws ItemStreamException {
		if (this.delegate instanceof ItemStream) {
			((ItemStream) this.delegate).open(context);
		}
	}

	public void update(ExecutionContext context) throws ItemStreamException {
		if (this.delegate instanceof ItemStream) {
			((ItemStream) this.delegate).update(context);
		}
	}

	// Commented since we have
	/*
	 * @Override public ExitStatus afterStep(StepExecution se) { // TODO
	 * Auto-generated method stub return se.getExitStatus(); }
	 * 
	 * @Override public void beforeStep(StepExecution se) {
	 * 
	 * this.stepExecution = se;
	 * 
	 * }
	 */

}