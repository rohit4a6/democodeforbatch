package edu.msstate.nsparc.batch.util;

import org.springframework.batch.item.file.ResourceSuffixCreator;

public class OutputFileSuffixCreator implements ResourceSuffixCreator{

	public String getSuffix(int arg0) {
		return arg0 + ".csv";
	}
}