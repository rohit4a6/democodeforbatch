package edu.msstate.nsparc.batch.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesUtil {

	private static Properties configProp = new Properties();
	private static Logger logger = Logger.getLogger(PropertiesUtil.class);

	/**
	 * Method to return the property value based on the key passed.
	 * 
	 * @param key
	 * @return String value
	 * @throws PropertyNotFoundException
	 */
	public static String getProperty(String key)
			throws PropertyNotFoundException {

		String propertyValue = configProp.getProperty(key);

		if (propertyValue == null || propertyValue.trim().length() == 0) {
			throw new PropertyNotFoundException();
		}

		return propertyValue;
	}
	
	/**
	 * Method to load the Application.properties using specified path argument
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */	
	public static void initializeProperties(String filepath)
			throws FileNotFoundException, IOException {

		logger.info("initializing Properties from " + filepath + " ==> Start");
		InputStream is = null;
		
		try {			
			is = new FileInputStream(filepath);
			configProp.load(is);
		} finally {
			if (is != null)
				is.close();
		}
		

		logger.info("initializing Properties ==> Complete");
		

	}

}
