package edu.msstate.nsparc.batch.writer;

import java.util.Iterator;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.JdbcTemplate;

import edu.msstate.nsparc.batch.info.WorkflowItemInfo;

public class WorkflowItemWriter<T> implements ItemWriter<T>{
	
	private JdbcTemplate jdbcTemplate;	

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}

	public void write(List<? extends T> items) throws Exception {
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			WorkflowItemInfo info = (WorkflowItemInfo) itr.next();
			System.out.println(info.toString());
		}
		
	}

}
