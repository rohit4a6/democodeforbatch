package edu.msstate.nsparc.batch.info;

import java.io.Serializable;

public class WorkflowItemInfo implements ItemInfo, Serializable{
	
	private static final long serialVersionUID = 1L;
	
	String clientId;
	String ssn;
	String caseId;
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
	
	
}
