package edu.msstate.nsparc.batch.info;

public class BatchInputFields {

	String clientId;

	String caseId;

	String ssn;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	@Override
	public String toString() {
		return "BatchInputFields [clientId=" + clientId + ", caseId=" + caseId + ", ssn=" + ssn + "]";
	}

}
