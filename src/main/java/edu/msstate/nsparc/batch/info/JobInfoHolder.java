package edu.msstate.nsparc.batch.info;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("jobInfoHolder")
@Scope("prototype")
public class JobInfoHolder implements ItemInfo {

	String clientId;

	String caseId;

	String ssn;

	String jobName;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	@Override
	public String toString() {
		return "JobInfoHolder [clientId=" + clientId + ", caseId=" + caseId + ", ssn=" + ssn + "]";
	}

}
