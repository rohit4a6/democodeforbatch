package edu.msstate.nsparc.batch.info;

import java.io.Serializable;

public class BatchDto implements Serializable {

	private static final long serialVersionUID = -2342299715412416057L;

	private String contextXmlName;

	private String batchJobName;

	private String propertiesFileName;

	private String jobAction;

	private String logFileName;

	private String restartJobId;

	private String tempFolderPath;

	private String restartTempDir;

	public String getContextXmlName() {
		return contextXmlName;
	}

	public void setContextXmlName(final String contextXmlName) {
		this.contextXmlName = contextXmlName;
	}

	public String getBatchJobName() {
		return batchJobName;
	}

	public void setBatchJobName(final String batchJobName) {
		this.batchJobName = batchJobName;
	}

	public String getPropertiesFileName() {
		return propertiesFileName;
	}

	public void setPropertiesFileName(final String propertiesFileName) {
		this.propertiesFileName = propertiesFileName;
	}

	public String getJobAction() {
		return jobAction;
	}

	public void setJobAction(final String jobAction) {
		this.jobAction = jobAction;
	}

	public String getLogFileName() {
		return logFileName;
	}

	public void setLogFileName(final String logFileName) {
		this.logFileName = logFileName;
	}

	public String getRestartJobId() {
		return restartJobId;
	}

	public void setRestartJobId(final String restartJobId) {
		this.restartJobId = restartJobId;
	}

	public String getTempFolderPath() {
		return tempFolderPath;
	}

	public void setTempFolderPath(final String tempFolderPath) {
		this.tempFolderPath = tempFolderPath;
	}

	public String getRestartTempDir() {
		return restartTempDir;
	}

	public void setRestartTempDir(final String restartTempDir) {
		this.restartTempDir = restartTempDir;
	}

}
