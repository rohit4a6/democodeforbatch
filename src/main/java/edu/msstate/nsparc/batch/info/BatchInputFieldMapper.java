package edu.msstate.nsparc.batch.info;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

public class BatchInputFieldMapper implements FieldSetMapper<BatchInputFields> {

	public BatchInputFields mapFieldSet(FieldSet fieldSet){

		BatchInputFields inputRecord = new BatchInputFields();

		inputRecord.setClientId(fieldSet.readString(0));

		inputRecord.setCaseId(fieldSet.readString(1));

		inputRecord.setSsn(fieldSet.readString(2));

		return inputRecord;

	}

}