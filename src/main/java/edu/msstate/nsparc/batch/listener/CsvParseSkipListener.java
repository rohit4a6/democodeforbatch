package edu.msstate.nsparc.batch.listener;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.springframework.batch.core.SkipListener;

public class CsvParseSkipListener implements SkipListener<Object, Object> {

	private static final Logger logger = Logger.getLogger("failAppender");

	private String convertThrowableToString(Throwable t) {

		StringWriter buffer = new StringWriter();
		PrintWriter temp = new PrintWriter(buffer);
		t.printStackTrace(temp);

		return buffer.toString();

	}

	@Override
	public void onSkipInRead(Throwable t) {
		logger.error("Encountered error while reading : " + convertThrowableToString(t));
	}

	@Override
	public void onSkipInWrite(Object item, Throwable t) {
		logger.error("Skipping writing : " + item.toString());
		logger.error("Encountered error while writing : " + convertThrowableToString(t));
	}

	@Override
	public void onSkipInProcess(Object item, Throwable t) {
		logger.error("Skipping processing item" + item.toString());
		logger.error("Encountered error while processing : " + convertThrowableToString(t));

	}

}
