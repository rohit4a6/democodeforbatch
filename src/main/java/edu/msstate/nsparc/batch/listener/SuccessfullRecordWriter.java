package edu.msstate.nsparc.batch.listener;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemWriteListener;

import edu.msstate.nsparc.batch.info.ItemInfo;

public class SuccessfullRecordWriter implements ItemWriteListener<Object>{

	private static final Logger successLogger = Logger.getLogger("successAppender");
	private static final Logger errorLogger = Logger.getLogger("failAppender");
	
	private String convertThrowableToString(Throwable t) {
		
		StringWriter buffer = new StringWriter();
		PrintWriter temp = new PrintWriter(buffer);
		t.printStackTrace(temp);
		
		return buffer.toString();
		
	}
	
	@Override
	public void afterWrite(List<? extends Object> infoList) {
		Iterator itr = infoList.iterator();
		while(itr.hasNext()){
			ItemInfo itemInfo = (ItemInfo)itr.next();
			if(itemInfo != null)
				successLogger.info(itemInfo.toString());				
		}
	}
	
	@Override
	public void beforeWrite(List<? extends Object> arg0) {
	}

	@Override
	public void onWriteError(Exception arg0, List<? extends Object> infoList) {
		Iterator itr = infoList.iterator();
		while(itr.hasNext()){
			ItemInfo itemInfo = (ItemInfo)itr.next();
			if(itemInfo != null)
				errorLogger.error("Encountered error while writing: "+ itemInfo.toString());
			
			errorLogger.error("Encountered error while writing: "+ convertThrowableToString(arg0));
		}
	}
	
	
}
