package edu.msstate.nsparc.batch.listener;

import org.apache.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

@Component("batchJobExecutionListener")
public class BatchJobExecutionListener implements JobExecutionListener {

	private static final Logger logger = Logger.getLogger(BatchJobExecutionListener.class);
	private static final Logger successLogger = Logger.getLogger("successAppender");
	private static final Logger failLogger = Logger.getLogger("failAppender");

	@Override
	public void afterJob(JobExecution jobexecution) {

		if (jobexecution.getStatus() == BatchStatus.STOPPED) {

			jobexecution.setStatus(BatchStatus.COMPLETED);
			jobexecution.setExitStatus(ExitStatus.COMPLETED);

		}
		logger.info("After Job Exit Status :" + jobexecution.getExitStatus());
		logger.info("After Job Status : " + jobexecution.getStatus());
		logger.info("After Job ExceptionList : " + jobexecution.getAllFailureExceptions());
	}

	@Override
	public void beforeJob(JobExecution jobexecution) {

		logger.info("******** Job " + jobexecution.getJobInstance().getJobName() + " started at "
				+ jobexecution.getStartTime());

		successLogger.info("******** Job " + jobexecution.getJobInstance().getJobName() + " started at "
				+ jobexecution.getStartTime());

		failLogger.error("******** Job " + jobexecution.getJobInstance().getJobName() + " started at "
				+ jobexecution.getStartTime());

	}

}
