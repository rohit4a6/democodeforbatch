package edu.msstate.nsparc.batch.config;

import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import edu.msstate.nsparc.batch.tasks.MigrationTask;

@Configuration
@EnableScheduling
@ComponentScan("edu.msstate.nsparc.batch.tasks")
public class JobCronConfiguration implements SchedulingConfigurer{

	private String cronTime ="*/10 * * * * *";
	
	private String zone="CST";
	
	@Autowired
	MigrationTask migrationTask;
	
	@Bean
	CronTrigger cronTrigger(){
		return new CronTrigger(cronTime, TimeZone.getTimeZone(zone));
	}
	
	@Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addCronTask(new CronTask(migrationTask, cronTrigger()));
    }
	
}
