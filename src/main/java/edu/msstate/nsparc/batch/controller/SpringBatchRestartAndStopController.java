package edu.msstate.nsparc.batch.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.DuplicateJobException;
import org.springframework.batch.core.launch.JobExecutionNotRunningException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.msstate.nsparc.batch.info.BatchDto;
import edu.msstate.nsparc.batch.tasks.RestartAndStopTask;

@RestController
@RequestMapping("/batch")
public class SpringBatchRestartAndStopController {

	@Autowired
	RestartAndStopTask restartAndStopTask;

	@RequestMapping(value = "/restart", consumes = APPLICATION_JSON_VALUE)
	public void restartBatch(@RequestBody final BatchDto batchDto) throws JobExecutionAlreadyRunningException,
			JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException, DuplicateJobException {
		restartAndStopTask.restartJob(batchDto);
	}
	
	@RequestMapping(value = "/stop", consumes = APPLICATION_JSON_VALUE)
	public void stop(@RequestBody final BatchDto batchDto) throws JobExecutionAlreadyRunningException,
			JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException, NoSuchJobExecutionException, JobExecutionNotRunningException, DuplicateJobException {
		restartAndStopTask.stopJob(batchDto);
	}

}
