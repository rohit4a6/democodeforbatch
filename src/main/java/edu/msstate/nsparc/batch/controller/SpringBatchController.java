/*package edu.msstate.nsparc.batch.controller;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.batch.core.launch.JobExecutionNotRunningException;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.support.CommandLineJobRunner;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.msstate.nsparc.batch.util.PropertiesUtil;

public class SpringBatchController {

	private static final Logger logger = Logger
			.getLogger(SpringBatchController.class);

	public static void main(String[] args) throws IOException {
		
		System.out.println("Inside SpringBatchController class");
		final String fileName = "D:/repositoryDemo/SpringBatch/BatchJob_log4j.properties";

		String jobContextFile = args[0];
		String jobName  = args[1];
		String propertiesFile = args[2];
		String jobControl = args[3];		
		
		System.getProperties().setProperty("propfileName", propertiesFile);	
		System.getProperties().setProperty("batchJobName", jobName);
		
		PropertyConfigurator.configure(fileName);
		
		logger.info("Logger initialized");		
		logger.info("property file name : "+ propertiesFile);
		logger.info("job context file name : "+ jobContextFile);
		logger.info("job name : "+ jobName);
		
		try 
		{
			PropertiesUtil.initializeProperties(propertiesFile);
		} catch (Exception e) {
			logger.error("Error trying to retrieve props: ",e);
			logger.error("Exception trying to retrieve props: " + e.getMessage());
			System.out.println("Exception trying to retrieve props: "+e.getMessage());
			return;
		}
		
		try
		{
			String tempFolderPath = PropertiesUtil.getProperty("CSV_FILE_PATH");
			String tempDirName = "";
			if (jobControl.equals("-next"))
			{
				long timeNow = System.currentTimeMillis();
				tempDirName = jobName+"_"+Long.toString(timeNow);
				
				File file = new File(tempFolderPath+File.separator+tempDirName);			 
				boolean b = false;			 
				 
				if (!file.exists()) 
				{			
					logger.info("Creating new directory "+tempDirName); 
					b = file.mkdirs();
					
					if (b)
					{
						 System.out.println("Directory "+ file.getAbsolutePath() + " successfully created");
						 System.getProperties().setProperty("tempFolderPath", file.getAbsolutePath()+File.separator);
					 }
					 else
					 {
						 System.out.println("Failed to create directory");
						 return;
					 }
				}
				
				CommandLineJobRunner.main(new String []{jobContextFile, jobName, jobControl});
				
			}
			else if (jobControl.equals("-restart"))
			{
				String restartExecutionId = args.length > 4 ? args[4] : null;
				String restartTempDir = args.length > 5 ? args[5] : null;
				
				tempDirName = restartTempDir;
				
				File file = new File(tempFolderPath+File.separator+tempDirName);			 
				//boolean b = false;			 
				 
				if (file.exists())
				{
					System.out.println("Found existing directory : "+tempDirName);
					System.getProperties().setProperty("tempFolderPath", file.getAbsolutePath()+File.separator);					 
				}
				else
				 {
					 System.out.println("Unable to locate directory : "+tempDirName);
					 return;
				 }
				
				CommandLineJobRunner.main(new String[] { jobContextFile, restartExecutionId, "-restart"});
				
			}
			else if (jobControl.equals("-stop"))
			{
				System.getProperties().setProperty("tempFolderPath", "");
				ConfigurableApplicationContext context = null;
				try {
					context = new AnnotationConfigApplicationContext(
							Class.forName(jobContextFile));
				} catch (ClassNotFoundException cnfe) {
					context = new ClassPathXmlApplicationContext(jobContextFile);
				}
				JobOperator jo = (SimpleJobOperator) context
						.getBean("jobOperator");

				try {
					Set<Long> runningExecs = jo.getRunningExecutions(jobName);
					Iterator<Long> itr = runningExecs.iterator();
					while (itr.hasNext()) {
						Long executionId = (Long) itr.next();
						try
						{
							logger.info("Sending stop signal to job execution id "
									+ executionId.toString());
							boolean stopMessageSent = jo.stop(executionId);
							logger.info("Stop signal sent to job execution id "
									+ executionId.toString() + " success : "
									+ stopMessageSent);
						}
						catch (JobExecutionNotRunningException e) {
							logger.error("Exception when stopping job with id "+ executionId);
							System.out.println("Exception when stopping job with id "+ executionId);
							
						}
						//System.out.println("Stop" + stopMessageSent);
					}

				} catch (NoSuchJobException e) {
					logger.error("There are no jobs running for "+jobName);
					System.out.println("There are no jobs running for "+jobName);
					
				}
				
			}
			
		} 
		catch (Exception e) {
			logger.error("Exception while runnning job : ",e);
			System.out.println("Exception while runnning job : "+e.getMessage());
			e.printStackTrace();
			return;
		}				
		
	}

}
*/