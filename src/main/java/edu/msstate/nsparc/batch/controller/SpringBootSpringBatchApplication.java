package edu.msstate.nsparc.batch.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan("edu.msstate.nsparc.batch")
@ImportResource({"jobContext.xml","BatchHelper.xml","csvHelper.xml"})
public class SpringBootSpringBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSpringBatchApplication.class, args);
	}
}