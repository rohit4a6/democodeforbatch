package edu.msstate.nsparc.batch.tasks;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.support.MapJobRegistry;
import org.springframework.batch.core.configuration.support.ReferenceJobFactory;
import org.springframework.batch.core.launch.JobExecutionNotRunningException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import edu.msstate.nsparc.batch.util.PropertiesUtil;

@Component
public class MigrationTask implements Runnable{
	
	private static final Logger logger = LoggerFactory.getLogger(MigrationTask.class);
	
	@Value("${job.context.xml}")
	private String jobContextFile;

	@Value("${job.name}")
	private String jobName;
	
	@Value("${job.properties.file.name}")
	private String propertiesFileName;
	
	@Value("${job.action}")
	private String action;
	
	@Value("${job.log.file.name}")
	private String logFileName;
	
	@Value("${job.restartId}")
	private String restartJobId;
	
	@Value("${job.restart.temp.dir}")
	private String restartTempDir;

	@Autowired
    ApplicationContext context;

	@SuppressWarnings("resource")
	@Override
	public void run(){
		System.getProperties().setProperty("propfileName", propertiesFileName);	
		System.getProperties().setProperty("batchJobName", jobName);
		
		PropertyConfigurator.configure(logFileName);
		
		logger.info("Logger initialized");		
		logger.info("property file name : "+ propertiesFileName);
		logger.info("job context file name : "+ jobContextFile);
		logger.info("job name : "+ jobName);
		
		try 
		{
			PropertiesUtil.initializeProperties(propertiesFileName);
		} catch (Exception e) {
			logger.error("Error trying to retrieve props: ",e);
			logger.error("Exception trying to retrieve props: " + e.getMessage());
			System.out.println("Exception trying to retrieve props: "+e.getMessage());
			return;
		}
		
		try
		{
			String tempFolderPath = PropertiesUtil.getProperty("CSV_FILE_PATH");
			String tempDirName = "";
			if (action.equals("-next"))
			{
				long timeNow = System.currentTimeMillis();
				tempDirName = jobName+"_"+Long.toString(timeNow);
				
				File file = new File(tempFolderPath+File.separator+tempDirName);			 
				boolean b = false;			 
				 
				if (!file.exists()) 
				{			
					logger.info("Creating new directory "+tempDirName); 
					b = file.mkdirs();
					
					if (b)
					{
						 System.out.println("Directory "+ file.getAbsolutePath() + " successfully created");
						 System.getProperties().setProperty("tempFolderPath", file.getAbsolutePath()+File.separator);
					 }
					 else
					 {
						 System.out.println("Failed to create directory");
						 return;
					 }
				}
				JobLauncher jobLauncher = context.getBean(JobLauncher.class);
				Job job = context.getBean(jobName, Job.class);
				
				MapJobRegistry mapJobRegistry = context.getBean("jobRegistry", MapJobRegistry.class);
				mapJobRegistry.unregister(jobName);
				mapJobRegistry.register(new ReferenceJobFactory(job));
				
				JobParameters jobParameters = new JobParametersBuilder().addString("name", jobName).addDate("date", new Date()).addLong("time",System.currentTimeMillis()).toJobParameters();
				jobLauncher.run(job, jobParameters);
				
				//CommandLineJobRunner.main(new String []{jobContextFile, jobName, action});
				
			}
			else if (action.equals("-restart"))
			{
				String restartExecutionId = restartJobId;
				
				tempDirName = restartTempDir;
				
				File file = new File(tempFolderPath+File.separator+tempDirName);			 
				 
				if (file.exists())
				{
					System.out.println("Found existing directory : "+tempDirName);
					System.getProperties().setProperty("tempFolderPath", file.getAbsolutePath()+File.separator);					 
				}
				else
				 {
					 System.out.println("Unable to locate directory : "+tempDirName);
					 return;
				 }
				
				JobLauncher jobLauncher = context.getBean(JobLauncher.class);
				Job job = context.getBean(jobName, Job.class);
				
				JobParameters jobParameters = new JobParametersBuilder().addString("jobId", jobName).addString("-restart", "-restart").addString("restartExecutionId", restartExecutionId)
						.addDate("date", new Date()).addLong("time",System.currentTimeMillis()).toJobParameters();
				jobLauncher.run(job, jobParameters);
				
				//CommandLineJobRunner.main(new String[] { jobContextFile, restartExecutionId, "-restart"});
				
			}
			else if (action.equals("-stop"))
			{
				System.getProperties().setProperty("tempFolderPath", "");
				ConfigurableApplicationContext context = null;
				try {
					context = new AnnotationConfigApplicationContext(
							Class.forName(jobContextFile));
				} catch (ClassNotFoundException cnfe) {
					context = new ClassPathXmlApplicationContext(jobContextFile);
				}
				JobOperator jo = (SimpleJobOperator) context
						.getBean("jobOperator");

				try {
					Set<Long> runningExecs = jo.getRunningExecutions(jobName);
					Iterator<Long> itr = runningExecs.iterator();
					while (itr.hasNext()) {
						Long executionId = (Long) itr.next();
						try
						{
							logger.info("Sending stop signal to job execution id "
									+ executionId.toString());
							boolean stopMessageSent = jo.stop(executionId);
							logger.info("Stop signal sent to job execution id "
									+ executionId.toString() + " success : "
									+ stopMessageSent);
						}
						catch (JobExecutionNotRunningException e) {
							logger.error("Exception when stopping job with id "+ executionId);
							System.out.println("Exception when stopping job with id "+ executionId);
							
						}
					}

				} catch (NoSuchJobException e) {
					logger.error("There are no jobs running for "+jobName);
					System.out.println("There are no jobs running for "+jobName);
					
				}
				
			}
			
		} 
		catch (Exception e) {
			logger.error("Exception while runnning job : ",e);
			System.out.println("Exception while runnning job : "+e.getMessage());
			e.printStackTrace();
			return;
		}
		
	}
	
}

