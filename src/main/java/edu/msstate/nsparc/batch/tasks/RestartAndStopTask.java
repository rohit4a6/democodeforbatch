package edu.msstate.nsparc.batch.tasks;

import java.io.File;
import java.util.Date;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.DuplicateJobException;
import org.springframework.batch.core.configuration.support.MapJobRegistry;
import org.springframework.batch.core.configuration.support.ReferenceJobFactory;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobExecutionNotRunningException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import edu.msstate.nsparc.batch.info.BatchDto;

@Component
public class RestartAndStopTask {
	
	private static final Logger logger = LoggerFactory.getLogger(RestartAndStopTask.class);
	
	@Autowired
	JobExplorer jobExplorer;

	@Autowired
	ApplicationContext appContext;
	
	public void restartJob(final BatchDto batchDto) throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException, DuplicateJobException {

		File file = new File(batchDto.getTempFolderPath()+File.separator+batchDto.getRestartTempDir());			 
		 
		if (file.exists())
		{
			System.out.println("Found existing directory : "+batchDto.getRestartTempDir());
			System.getProperties().setProperty("tempFolderPath", file.getAbsolutePath()+File.separator);					 
		}
		else
		 {
			 System.out.println("Unable to locate directory : "+batchDto.getRestartTempDir());
			 return;
		 }
		
		JobLauncher jobLauncher = appContext.getBean(JobLauncher.class);
		
		Job job = appContext.getBean(batchDto.getBatchJobName(), Job.class);
		
		MapJobRegistry mapJobRegistry = appContext.getBean("jobRegistry", MapJobRegistry.class);
		mapJobRegistry.unregister(batchDto.getBatchJobName());
		mapJobRegistry.register(new ReferenceJobFactory(job));
		
		JobParameters jobParameters = new JobParametersBuilder().addString("jobId", batchDto.getBatchJobName())
				.addString("-restart", "-restart").addString("restartExecutionId", batchDto.getRestartJobId())
				.addDate("date", new Date()).addLong("time",System.currentTimeMillis()).toJobParameters();
		jobLauncher.run(job, jobParameters);
		
	}
	
	@SuppressWarnings("resource")
	public void stopJob(final BatchDto batchDto) throws NoSuchJobExecutionException, JobExecutionNotRunningException, DuplicateJobException{
		System.getProperties().setProperty("tempFolderPath", "");
		ConfigurableApplicationContext context = null;
		try {
			context = new AnnotationConfigApplicationContext(
					Class.forName(batchDto.getContextXmlName()));
		} catch (ClassNotFoundException cnfe) {
			context = new ClassPathXmlApplicationContext(batchDto.getContextXmlName());
		}
		JobOperator jo = (SimpleJobOperator) context
				.getBean("jobOperator");
		Job job = context.getBean(batchDto.getBatchJobName(), Job.class);
		
		MapJobRegistry mapJobRegistry = context.getBean("jobRegistry", MapJobRegistry.class);
		mapJobRegistry.unregister(batchDto.getBatchJobName());
		mapJobRegistry.register(new ReferenceJobFactory(job));

		Set<JobExecution> jobExecutionsSet= jobExplorer.findRunningJobExecutions(batchDto.getBatchJobName());
		for (JobExecution jobExecution:jobExecutionsSet) {
		    System.out.println(jobExecution.getStatus()+"ID :"+jobExecution.getId());
		    if (jobExecution.getStatus()== BatchStatus.STARTED|| jobExecution.getStatus()== BatchStatus.STARTING){
		    	logger.info("Sending stop signal to job execution id "
						+ jobExecution.getId().toString());
				boolean stopMessageSent = jo.stop(jobExecution.getId());
				logger.info("Stop signal sent to job execution id "
						+ jobExecution.getId().toString() + " success : "
						+ stopMessageSent);
		    }
		}
	}

}
